/*
Navicat MySQL Data Transfer

Source Server         : lan_server
Source Server Version : 50730
Source Host           : 192.168.31.84:3306
Source Database       : uninotify

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-07-15 09:32:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for uninotify_developer
-- ----------------------------
DROP TABLE IF EXISTS `uninotify_developer`;
CREATE TABLE `uninotify_developer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(255) DEFAULT NULL,
  `notify_url` varchar(255) DEFAULT NULL,
  `app_id` varchar(255) NOT NULL,
  `app_secret` varchar(255) NOT NULL,
  `gmt_update` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `gmt_create` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for uninotify_developer_user
-- ----------------------------
DROP TABLE IF EXISTS `uninotify_developer_user`;
CREATE TABLE `uninotify_developer_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `open_id` varchar(255) NOT NULL,
  `gmt_update` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `gmt_create` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`app_id`,`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1210417372139696130 DEFAULT CHARSET=utf8mb4;
